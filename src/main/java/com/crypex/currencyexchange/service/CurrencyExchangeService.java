package com.crypex.currencyexchange.service;

import com.crypex.currencyexchange.model.CurrencyExchange;
import com.crypex.currencyexchange.model.CurrencyExchangeDto;

public interface CurrencyExchangeService {
	public CurrencyExchange getById(Long id);
	public CurrencyExchangeDto getCurrencyInformationByCurrency(String currency);
	
}
