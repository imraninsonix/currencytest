package com.crypex.currencyexchange.controller;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.logging.Logger;

import org.apache.catalina.servlet4preview.http.HttpServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.servlet.ModelAndView;

import com.coinbase.api.Coinbase;
import com.coinbase.api.exception.CoinbaseException;
import com.crypex.currencyexchange.model.CurrencyExchangeDto;
import com.crypex.currencyexchange.service.CurrencyExchangeService;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;


@RestController
@RequestMapping(value="/currency")
public class CurrencyExchangeController {
	
	@Autowired
	private CurrencyExchangeService currencyExchangeService;
	
	@Autowired
	ObjectMapper objectMapper;
	
	protected Logger logger = Logger.getLogger(CurrencyExchangeController.class
			.getName());
	
	private Coinbase cb;
	private String forTransaction;
	
	private String clientId = "093f60c5b83ea7e1820cbdb4aa2029843bd5fc0188372342c66c4bf589297604";
	private String clientsecretKey = "9445a2261010265fcf2d6787bf7100e0327375326575012a5d9e2e4e5f2ad039";
	private String redirectURL  = "https://ec2-52-15-244-59.us-east-2.compute.amazonaws.com:8443/currency-exchange/currency/abc";
	private String secureRandom = "secureRandom";
	private String scope = "wallet:accounts:read,wallet:transactions:read,wallet:deposits:create";
	
	
	public CurrencyExchangeController(CurrencyExchangeService currencyExchangeService) {
		this.currencyExchangeService = currencyExchangeService;
	}
	/**
	 * Callback Method called after request is successfully completed
	 * @return
	 */
	@RequestMapping(value="/abc", method=RequestMethod.GET)
	public ModelAndView callback(HttpServletRequest request){
		
		ModelAndView modelAndView = new ModelAndView("hello");
		String code = request.getParameter("code");
		try {
			String accessToken = getAccessToken(code);
			
			forTransaction = request.getParameter("forTransaction");
			forTransaction = "false";
			
			JsonNode accountData = getAccountInfo(accessToken);
			JsonNode userData = getUserDetail(accessToken);
			
			String accountId = accountData.get(0).get("id").textValue();
			/*JsonNode depositData = deposit(accountId, accessToken);*/
			
			if(forTransaction.equals("true")){
				String transactionData = doTransaction(accountId, accessToken);
				modelAndView.addObject("result", transactionData);
			}
			else{
				String userId = userData.get("id").textValue();
				modelAndView.addObject("accountData", accountData);
				modelAndView.addObject("userData", userData);
				/*modelAndView.addObject("depositData", depositData);*/
				
			}
		} catch (IOException | CoinbaseException e) {
			e.printStackTrace();
		}
		return modelAndView;
	}
	/**
	 * THe method to call for user authorization
	 * @param clientId
	 * @param redirectURL
	 * @param secureRandom
	 * @param scope
	 * @return
	 */
	@RequestMapping("test/authorizeApplication")
	private ModelAndView authorizeApplication(String clientId, String redirectURL, String secureRandom, String scope){
		
		ModelAndView mav = new ModelAndView("authorizeApplication");
		
		String url = "https://www.coinbase.com/oauth/authorize?response_type=code&client_id="+this.clientId+"&redirect_uri="+this.redirectURL+"&state="+this.secureRandom+"&scope="+this.scope+"";
		
		//url = forTransaction ? url + ",wallet:transactions:send&meta[send_limit_amount]=0.0000003&meta[send_limit_currency]=USD&meta[send_limit_period]=month" : url;
		
		RestTemplate template = new RestTemplate();
		String codePage = template.getForObject(url, String.class);
		/*mav.addObject("codePage", codePage);*/
		return mav;
	}
	
	@RequestMapping("exchange/{currency}")
	public CurrencyExchangeDto getCurrencyInformationByCurrency(@PathVariable String currency) {
		logger.info("ACCESSED");
		return currencyExchangeService.getCurrencyInformationByCurrency(currency);
	}
	
	private String getAccessToken(String code) throws JsonProcessingException, IOException{
		String url = "https://api.coinbase.com/oauth/token";
		
		 MultiValueMap<String, String> headers = new LinkedMultiValueMap<String, String>();
	        Map<String, String> map = new HashMap<String, String>();
	        map.put("Content-Type", "application/json");

	        headers.setAll(map);

	        Map<String, Object> req_payload = new HashMap();
	        req_payload.put("grant_type", "authorization_code");
	        req_payload.put("code", code);
	        req_payload.put("client_id", this.clientId);
	        req_payload.put("client_secret", this.clientsecretKey);
	        req_payload.put("redirect_uri", this.redirectURL);
	        
	        String accessToken = null;
	        try{
	        HttpEntity<?> request = new HttpEntity<>(req_payload, headers);
	        ResponseEntity<String> response = new RestTemplate().postForEntity(url, request, String.class);
	        
			JsonNode jsonNode = objectMapper.readTree(response.getBody());
			accessToken = jsonNode.get("access_token").textValue();
	        }catch(Exception e){
	        	e.printStackTrace();
	        }
	        return accessToken;
	}
	
	private JsonNode  getUserDetail(String authenticationToken) throws IOException, CoinbaseException{
		HttpHeaders headers = new HttpHeaders();
		headers.set("Content-Type", "application/json");
		headers.set("Authorization", "Bearer " + authenticationToken);
		HttpEntity entity = new HttpEntity(headers);
		
		String url = "https://api.coinbase.com/v2/user/";

		ResponseEntity<String> response = new RestTemplate().exchange(url, HttpMethod.GET, entity, String.class);
		
		JsonNode jsonNode = objectMapper.readTree(response.getBody()).get("data");
		
		/*cb = new CoinbaseBuilder().withBaseApiURL(new URL("https://api.coinbase.com/v2/"))
                .withAccessToken(authenticationToken)
                .build();
		User userdetail = cb.getUser();*/
		return jsonNode;
}
	private JsonNode getAccountInfo(String authenticationToken) throws IOException, CoinbaseException{
		
		HttpHeaders headers = new HttpHeaders();
		headers.set("Content-Type", "application/json");
		headers.set("Authorization", "Bearer " + authenticationToken);
		HttpEntity entity = new HttpEntity(headers);
		
		String url = "https://api.coinbase.com/v2/accounts";

		ResponseEntity<String> response = new RestTemplate().exchange(url, HttpMethod.GET, entity, String.class);
		
			JsonNode jsonNode = objectMapper.readTree(response.getBody()).get("data");
		
		return jsonNode;
	}
	
	private String doTransaction(String account_id, String authenticationToken) throws IOException, CoinbaseException{
		
		MultiValueMap<String, String> headers = new LinkedMultiValueMap<String, String>();
        Map<String, String> map = new HashMap<String, String>();
        map.put("Content-Type", "application/json");
        headers.set("Authorization", "Bearer " + authenticationToken);
        headers.setAll(map);

        Map<String, Object> req_payload = new HashMap();
        req_payload.put("type", "send");
        req_payload.put("to", "imran271190@gmail.com");
        req_payload.put("amount", "0.09");
        req_payload.put("currency", "USD");
        req_payload.put("description", "Test send");
        req_payload.put("idem", "Test1123");
        
        String url = " https://api.coinbase.com/v2/accounts/"+account_id+"/transactions";
        HttpEntity<?> request = new HttpEntity<>(req_payload, headers);
        ResponseEntity<String> response = new RestTemplate().postForEntity(url, request, String.class);
        
		JsonNode jsonNode = objectMapper.readTree(response.getBody());
		String accessToken = jsonNode.get("data").textValue();
        return accessToken;
}
	
	private JsonNode deposit(String accountId, String authenticationToken) throws JsonProcessingException, IOException{
		MultiValueMap<String, String> headers = new LinkedMultiValueMap<String, String>();
        Map<String, String> map = new HashMap<String, String>();
        map.put("Content-Type", "application/json");
        headers.set("Authorization", "Bearer " + authenticationToken);
        headers.setAll(map);

        Map<String, Object> req_payload = new HashMap();
        req_payload.put("type", "send");
        req_payload.put("to", "imran271190@gmail.com");
        req_payload.put("amount", "0.09");
        req_payload.put("currency", "USD");
        req_payload.put("description", "Test send");
        req_payload.put("idem", "Test1123");
        
        String url = " https://api.coinbase.com/v2/accounts/"+accountId+"/deposits";
        HttpEntity<?> request = new HttpEntity<>(req_payload, headers);
        ResponseEntity<String> response = new RestTemplate().postForEntity(url, request, String.class);
        
		JsonNode jsonNode = objectMapper.readTree(response.getBody());
		
		return jsonNode;
	}
	
	/*private void doTransaction(Coinbase cb) throws CoinbaseException, IOException{
		Transaction t = new Transaction();
		t.setTo("mpJKwdmJKYjiyfNo26eRp4j6qGwuUUnw9x");
		//t.setTo("satish.insonix@gmail.com"); 	
		t.setAmount(Money.parse("USD 2.23"));
		t.setNotes("Thanks for the coffee!");
		Transaction r = cb.sendMoney(t);
	}
	
	private void getCurrentTransactions(Coinbase cb) throws IOException, CoinbaseException{
	 * 
	 * We first need to get account id from https://api.coinbase.com/v2/accounts/ the pass in the account id to get 
	 * https://api.coinbase.com/v2/accounts/:account_id/transactions to get transactions
	 * 
	 * HttpHeaders headers = new HttpHeaders();
			headers.set("Content-Type", "application/json");
			headers.set("Authorization", "Bearer " + authenticationToken);
			HttpEntity entity = new HttpEntity(headers);
			
			String url = "https://api.coinbase.com/v2/accounts/:account_id/transactions";

			ResponseEntity<String> response = new RestTemplate().exchange(url, HttpMethod.GET, entity, String.class);
			
			JsonNode jsonNode = objectMapper.readTree(response.getBody());
			String userdata = jsonNode.get("data").textValue();
	 * 
	 * 
		//Sorted in descending order by createdAt, 30 transactions per page
		TransactionsResponse tr = cb.getTransactions();
		tr.getTotalCount(); 
		tr.getCurrentPage(); 
		tr.getNumPages(); 

		List<Transaction> txs = tr.getTransactions();
		txs.get(0).getId();
		TransactionsResponse page_2 = cb.getTransactions(2);
	}
	
	private void getSpecificTransactionDetails(String transactionId) throws IOException, CoinbaseException{
		transactionId = "5011f33df8182b142400000e";
		Transaction t = cb.getTransaction(transactionId);
		t.getStatus(); // Transaction.Status.PENDING
		t.getRecipientAddress(); // "mpJKwdmJKYjiyfNo26eRp4j6qGwuUUnw9x"
	}*/
	
	
	
	
}