package com.crypex.currencyexchange.model;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonProperty;
public class CurrencyExchangeDto implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = -235175554765291934L;
	
	@JsonProperty("id")
	private String currencyId;
	
	@JsonProperty("name")
	private String name;
	
	@JsonProperty("symbol")
	private String symbol;
	
	@JsonProperty("rank")
	private String rank;
	
	@JsonProperty("price_usd")
	private String priceUsd;
	
	@JsonProperty("price_btc")
	private String priceBtc;
	
	@JsonProperty("24h_volume_usd")
	private String volumeUsdPerDay;
	
	@JsonProperty("market_cap_usd")
	private String marketCapUsd;
	
	@JsonProperty("available_supply")
	private String availableSupply;
	
	@JsonProperty("total_supply")
	private String totalSupply;
	
	@JsonProperty("max_supply")
	private String maxSupply;
	
	@JsonProperty("percent_change_1h")
	private String percentChangePerHour;
	
	@JsonProperty("percent_change_24h")
	private String percentChangePerDay;
	
	@JsonProperty("percent_change_7d") 
	private String percentChangePerWeek;
	
	@JsonProperty("last_updated")
	private String lastUpdated;
	
	@JsonProperty("price_inr") 
	private String priceInr;
	
	@JsonProperty("24h_volume_inr")
	private String VolumeInrPerDay;
	
	@JsonProperty("market_cap_inr")
	private String marketCapInr;
	
	public String getCurrencyId() {
		return currencyId;
	}
	public void setCurrencyId(String currencyId) {
		this.currencyId = currencyId;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getSymbol() {
		return symbol;
	}
	public void setSymbol(String symbol) {
		this.symbol = symbol;
	}
	public String getRank() {
		return rank;
	}
	public void setRank(String rank) {
		this.rank = rank;
	}
	public String getPriceUsd() {
		return priceUsd;
	}
	public void setPriceUsd(String priceUsd) {
		this.priceUsd = priceUsd;
	}
	public String getPriceBtc() {
		return priceBtc;
	}
	public void setPriceBtc(String priceBtc) {
		this.priceBtc = priceBtc;
	}
	public String getVolumeUsdPerDay() {
		return volumeUsdPerDay;
	}
	public void setVolumeUsdPerDay(String volumeUsdPerDay) {
		this.volumeUsdPerDay = volumeUsdPerDay;
	}
	public String getMarketCapUsd() {
		return marketCapUsd;
	}
	public void setMarketCapUsd(String marketCapUsd) {
		this.marketCapUsd = marketCapUsd;
	}
	public String getAvailableSupply() {
		return availableSupply;
	}
	public void setAvailableSupply(String availableSupply) {
		this.availableSupply = availableSupply;
	}
	public String getTotalSupply() {
		return totalSupply;
	}
	public void setTotalSupply(String totalSupply) {
		this.totalSupply = totalSupply;
	}
	public String getMaxSupply() {
		return maxSupply;
	}
	public void setMaxSupply(String maxSupply) {
		this.maxSupply = maxSupply;
	}
	public String getPercentChangePerHour() {
		return percentChangePerHour;
	}
	public void setPercentChangePerHour(String percentChangePerHour) {
		this.percentChangePerHour = percentChangePerHour;
	}
	public String getPercentChangePerDay() {
		return percentChangePerDay;
	}
	public void setPercentChangePerDay(String percentChangePerDay) {
		this.percentChangePerDay = percentChangePerDay;
	}
	public String getPercentChangePerWeek() {
		return percentChangePerWeek;
	}
	public void setPercentChangePerWeek(String percentChangePerWeek) {
		this.percentChangePerWeek = percentChangePerWeek;
	}
	public String getLastUpdated() {
		return lastUpdated;
	}
	public void setLastUpdated(String lastUpdated) {
		this.lastUpdated = lastUpdated;
	}
	public String getPriceInr() {
		return priceInr;
	}
	public void setPriceInr(String priceInr) {
		this.priceInr = priceInr;
	}
	public String getVolumeInrPerDay() {
		return VolumeInrPerDay;
	}
	public void setVolumeInrPerDay(String volumeInrPerDay) {
		VolumeInrPerDay = volumeInrPerDay;
	}
	public String getMarketCapInr() {
		return marketCapInr;
	}
	public void setMarketCapInr(String marketCapInr) {
		this.marketCapInr = marketCapInr;
	}


}