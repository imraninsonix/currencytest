package com.crypex.currencyexchange.model;

import java.io.Serializable;
import java.math.BigInteger;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="CURRENCY_EXCHANGE")
public class CurrencyExchange implements Serializable{

	private static final long serialVersionUID = -205411139870367879L;

	private Long id;
	private String currencyId;
	private String name;
	private String symbol;
	private String rank;
	private String priceUsd;
	private String priceBtc;
	private String volumeUsdPerDay;
	private String marketCapUsd;
	private String availableSupply;
	private String totalSupply;
	private String maxSupply;
	private String percentChangePerHour;
	private String percentChangePerDay;
	private String percentChangePerWeek;
	private String lastUpdated;
	private String priceInr;
	private String VolumeInrPerDay;
	private String marketCapInr;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "CURRENCY_EXCHANGE_ID")
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	@Column(name = "CURRENCY_ID")
	public String getCurrencyId() {
		return currencyId;
	}
	public void setCurrencyId(String currencyId) {
		this.currencyId = currencyId;
	}
	@Column(name = "NAME")
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	@Column(name = "SYMBOL")
	public String getSymbol() {
		return symbol;
	}
	public void setSymbol(String symbol) {
		this.symbol = symbol;
	}
	@Column(name = "RANK")
	public String getRank() {
		return rank;
	}
	public void setRank(String rank) {
		this.rank = rank;
	}
	@Column(name = "PRICE_USD")
	public String getPriceUsd() {
		return priceUsd;
	}
	public void setPriceUsd(String priceUsd) {
		this.priceUsd = priceUsd;
	}
	@Column(name = "PRICE_BTC")
	public String getPriceBtc() {
		return priceBtc;
	}
	public void setPriceBtc(String priceBtc) {
		this.priceBtc = priceBtc;
	}
	@Column(name = "VOLUME_USD_PER_DAY")
	public String getVolumeUsdPerDay() {
		return volumeUsdPerDay;
	}
	public void setVolumeUsdPerDay(String volumeUsdPerDay) {
		this.volumeUsdPerDay = volumeUsdPerDay;
	}
	@Column(name = "MARKET_CAP_USD")
	public String getMarketCapUsd() {
		return marketCapUsd;
	}
	public void setMarketCapUsd(String marketCapUsd) {
		this.marketCapUsd = marketCapUsd;
	}
	@Column(name = "AVAILABLE_SUPPLY")
	public String getAvailableSupply() {
		return availableSupply;
	}
	public void setAvailableSupply(String availableSupply) {
		this.availableSupply = availableSupply;
	}
	@Column(name = "TOTAL_SUPPLY")
	public String getTotalSupply() {
		return totalSupply;
	}
	public void setTotalSupply(String totalSupply) {
		this.totalSupply = totalSupply;
	}
	@Column(name = "MAX_SUPPLY")
	public String getMaxSupply() {
		return maxSupply;
	}
	public void setMaxSupply(String maxSupply) {
		this.maxSupply = maxSupply;
	}
	@Column(name = "PERCENT_CHANGE_PER_HOUR")
	public String getPercentChangePerHour() {
		return percentChangePerHour;
	}
	public void setPercentChangePerHour(String percentChangePerHour) {
		this.percentChangePerHour = percentChangePerHour;
	}
	@Column(name = "PERCENT_CHANGE_PER_DAY")
	public String getPercentChangePerDay() {
		return percentChangePerDay;
	}
	public void setPercentChangePerDay(String percentChangePerDay) {
		this.percentChangePerDay = percentChangePerDay;
	}
	@Column(name = "PERCENT_CHANGE_PER_WEEK")
	public String getPercentChangePerWeek() {
		return percentChangePerWeek;
	}
	public void setPercentChangePerWeek(String percentChangePerWeek) {
		this.percentChangePerWeek = percentChangePerWeek;
	}
	@Column(name = "LAST_UPDATED")
	public String getLastUpdated() {
		return lastUpdated;
	}
	public void setLastUpdated(String lastUpdated) {
		this.lastUpdated = lastUpdated;
	}
	@Column(name = "PRICE_INR")
	public String getPriceInr() {
		return priceInr;
	}
	public void setPriceInr(String priceInr) {
		this.priceInr = priceInr;
	}
	@Column(name = "VOLUME_INR_PER_DAY")
	public String getVolumeInrPerDay() {
		return VolumeInrPerDay;
	}
	public void setVolumeInrPerDay(String volumeInrPerDay) {
		VolumeInrPerDay = volumeInrPerDay;
	}
	@Column(name = "MARKET_CAP_INR")
	public String getMarketCapInr() {
		return marketCapInr;
	}
	public void setMarketCapInr(String marketCapInr) {
		this.marketCapInr = marketCapInr;
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		CurrencyExchange other = (CurrencyExchange) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}

}